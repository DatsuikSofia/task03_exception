package com.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class UserMenu {
    static Result realize = new Result();
    public static int firstNumber;
    public static int lastNumber;

    public static void main(String arg[]) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter first number of interval");
        firstNumber = Integer.parseInt(reader.readLine());
        System.out.println("Enter last number of interval");
        lastNumber = Integer.parseInt(reader.readLine());
        try {
            printMenu();
        } catch (PerimeterExeption perimeterExeption) {
            perimeterExeption.printStackTrace();
        }
    }

    public static void printMenu() throws PerimeterExeption {
        Scanner num = new Scanner(System.in);
        System.out.println("Choose the operation");
        System.out.println("Enter '1' to print odd values");
        System.out.println("Enter '2' to print even values");
        System.out.println("Enter '3' to print the sum of odd numbers");
        System.out.println("Enter '4' to print the sum of even numbers");
        System.out.println("Enter '5' to enter size of Fibonacci");

        System.out.println("Enter '6' to print Fibonacci");
        System.out.println("Enter 7 to print percentage of odd numbers and even  Fibonacci numbers ");
        int value = num.nextInt();
        switch (value) {
            case 1:
                System.out.println(" Odd values of interval");
                System.out.println( " ") ;
                realize.printOddnumbers();

            case 2:
                System.out.println("Even values of interval");
                realize.printEvenNumbers();
                System.out.println( " ") ;
                printMenu();
            case 3:
                System.out.println("Print the sum of odd numbers");
                realize.summOddNumbers();
                System.out.println( " ") ;
                printMenu();
            case 4:
                System.out.println("Print the sum of even numbers");
                realize.summEvenNumbers();
                System.out.println( " ") ;
                printMenu();
            case 5:
                System.out.println(" Size of Fibonacci");
                realize.countFibonucciNumbers();
                System.out.println( " ") ;
                printMenu();
            case 6:
                System.out.println("Print Fibonacci");
                realize.fibonacciNumbers();
                printMenu();
            case 7:
                System.out.println("Print percentage of odd numbers and even  Fibonacci numbers ");
                realize.perceadeFibonacciNum();
                System.out.println( " ") ;
               printMenu();
            case 8:
                exit();


        }
        if(value > 8){
            throw new PerimeterExeption( " Your choose is incorect please enter another value");
        }
    }

    public static void exit() {
        System.exit(0);
    }


}



